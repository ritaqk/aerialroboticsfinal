#!/bin/sh
# Written by Rita Kambil April 2020


SESSION_NAME="Aerial"

tmux has-session -t ${SESSION_NAME}

if [ $? != 0 ]
then
  # Create the session
    tmux new-session -s ${SESSION_NAME} -d

    # First window (0) -- vim and console
    tmux send-keys -t ${SESSION_NAME} 'ls ' C-m
    
    tmux select-window -t ${SESSION_NAME}:0
    tmux split-window -h -p 60 -t ${SESSION_NAME}
    tmux send-keys -t ${SESSION_NAME}:0.1 'cd /home/aerial-robotics/Workspace/game-engine/bin && ./mediation_layer'  C-m 

    tmux select-window -t ${SESSION_NAME}:0
    tmux split-window -h -p 50 -t ${SESSION_NAME}:0
    tmux send-keys -t ${SESSION_NAME}:0.2 'cd /home/aerial-robotics/Workspace/game-engine/run  && rosparam load params.yaml /game_engine/ && rosrun rviz rviz -d config.rviz' C-m

    tmux select-window -t ${SESSION_NAME}:0
    tmux split-window -h -p 50 -t ${SESSION_NAME}:0
    tmux send-keys -t ${SESSION_NAME}:0.3 'cd /home/aerial-robotics/Workspace/game-engine/bin && ./physics_simulator' C-m

    tmux select-window -t ${SESSION_NAME}:0
    tmux split-window -h -p 50 -t ${SESSION_NAME}:0
    tmux send-keys -t ${SESSION_NAME}:0.4 'cd /home/aerial-robotics/Workspace/game-engine/bin && ./visualizer' C-m

    # Start out on the first window when we attach
    tmux select-pane -t ${SESSION_NAME}:0.0
fi
tmux attach -t ${SESSION_NAME}
