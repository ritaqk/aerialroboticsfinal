
#include <memory>
#include <vector>
#include <chrono>
#include <iostream>

#include "graph.h"
#include "timer.h"
#include "path_info3d.h"
namespace game_engine
{

    class NodeWrapper
    {
    public:
        using NodeWrapperPtr = std::shared_ptr<NodeWrapper>;

        NodeWrapperPtr parent;
        std::shared_ptr<Node3D> node_ptr;
        // True cost to this node
        double cost;

        // Heuristic to end node
        double heuristic;

        NodeWrapper() // initialize empty nodewrapper
        {
            this->parent = NULL;
            this->node_ptr = NULL;
            this->cost = 0;
            this->heuristic = 0;
        }

        double heuristic_func(
            const std::shared_ptr<Node3D> &current_ptr,
            const std::shared_ptr<Node3D> &end_ptr)
        {
            // Euclidean
            double x1, x2, y1, y2, z1, z2;
            x1 = current_ptr->Data().x();
            y1 = current_ptr->Data().y();
            z1 = current_ptr->Data().z();
            x2 = end_ptr->Data().x();
            y2 = end_ptr->Data().y();
            z2 = end_ptr->Data().z();

            return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1) + (z2 - z1) * (z2 - z1));
        }

        /*
    * A* Constructor with heuristic from node ptrs
    *
    * @param parent NodeWrapperPtr
    * @param node_ptr
    * @param cost
    * @param end_ptr

    * @return NodeWrapper
    */
        NodeWrapper(std::shared_ptr<NodeWrapper> parent,
                    std::shared_ptr<Node3D> node_ptr,
                    double cost,
                    std::shared_ptr<Node3D> end_ptr)
        {
            this->parent = parent;
            this->node_ptr = node_ptr;
            this->cost = cost;
            this->heuristic = heuristic_func(node_ptr, end_ptr);
        }

        /*
    * No Heuristic Constructor from node ptrs
    *
    * @param parent NodeWrapperPtr
    * @param node_ptr
    * @param cost

    * @return NodeWrapper object
    */
        NodeWrapper(std::shared_ptr<NodeWrapper> parent,
                    std::shared_ptr<Node3D> node_ptr,
                    double cost)
        {
            this->parent = parent;
            this->node_ptr = node_ptr;
            this->cost = cost;
            this->heuristic = 0;
        }

        /*
    * No Heuristic Constructor from DirectedEdge3D
    *
    * @param parent NodeWrapperPtr
    * @param edge DirectedEdge3D

    * @return NodeWrapper object
    */
        NodeWrapper(std::shared_ptr<NodeWrapper> parent,
                    DirectedEdge3D edge)
        {
            this->parent = parent;
            this->node_ptr = edge.Sink();
            this->cost = edge.Cost();
            this->heuristic = 0;
        }

        /*
    * Heuristic Constructor from DirectedEdge3D
    *
    * @param parent NodeWrapperPtr
    * @param edge DirectedEdge3D

    * @return NodeWrapper object
    */
        NodeWrapper(std::shared_ptr<NodeWrapper> parent,
                    DirectedEdge3D edge,
                    std::shared_ptr<Node3D> end_ptr)
        {
            this->parent = parent;
            this->node_ptr = edge.Sink();
            this->cost = edge.Cost();
            this->heuristic = heuristic_func(edge.Sink(), end_ptr);
        }

    public:
        // Equality operator
        bool operator==(const NodeWrapper &other) const
        {
            return *(this->node_ptr) == *(other.node_ptr);
        }
    };
} // namespace game_engine
