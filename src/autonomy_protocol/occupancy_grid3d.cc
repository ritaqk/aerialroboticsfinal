// Author: Tucker Haydon

#include "occupancy_grid3d.h"
#include <iostream>

#define EDGE_SAFETY 1
namespace game_engine
{

    OccupancyGrid3D::~OccupancyGrid3D()
    {
        if (false == this->heap_allocated_)
        {
            return;
        }

        // Deallocate memory on heap
        for (size_t idx = 0; idx < this->size_z_; ++idx)
        {
            for (size_t idx2 = 0; idx2 < this->size_y_; ++idx2)
            {
                std::free(this->data_[idx][idx2]);
            }
        }
        std::free(this->data_);
    }

    Eigen::Vector3d OccupancyGrid3D::Origin() const
    {
        return this->origin;
    }

    size_t OccupancyGrid3D::SizeX() const
    {
        return this->size_x_;
    }

    size_t OccupancyGrid3D::SizeY() const
    {
        return this->size_y_;
    }

    size_t OccupancyGrid3D::SizeZ() const
    {
        return this->size_z_;
    }

    double OccupancyGrid3D::GridSize() const
    {
        return this->grid_size;
    }

    bool OccupancyGrid3D::IsOccupied(const size_t z, const size_t y, const size_t x) const
    {
        return this->data_[z][y][x];
    }

    const bool ***OccupancyGrid3D::Data() const
    {
        return const_cast<const bool ***>(this->data_);
    }

    bool OccupancyGrid3D::LoadFromMap(
        const Map3D &map,
        const double sample_delta,
        const double safety_bound)
    {
        double min_x{std::numeric_limits<double>::max()},
            min_y{std::numeric_limits<double>::max()},
            min_z{std::numeric_limits<double>::max()},
            max_x{std::numeric_limits<double>::min()},
            max_y{std::numeric_limits<double>::min()},
            max_z{std::numeric_limits<double>::min()};

        //We are setting the limits of OccupancyGrid3D based on map3D boundaries.
        //map2D boundary is just given by a plane, which consist of vertices.
        //map3D boundary is given by multiple planes.  The planes can be obtained from Faces() method.

        for (const Plane3D &plane : map.Boundary().Faces())
        {
            for (const Line3D &edge : plane.Edges())
            {
                //Each line3d object has a start point and end point.
                for (int i = 0; i < 2; i++)
                {
                    Point3D vertex;
                    if (i == 0)
                        vertex = edge.Start();
                    else
                        vertex = edge.End();

                    if (vertex.x() < min_x)
                    {
                        min_x = vertex.x();
                    }
                    if (vertex.y() < min_y)
                    {
                        min_y = vertex.y();
                    }
                    if (vertex.z() < min_z)
                    {
                        min_z = vertex.z();
                    }
                    if (vertex.x() > max_x)
                    {
                        max_x = vertex.x();
                    }
                    if (vertex.y() > max_y)
                    {
                        max_y = vertex.y();
                    }
                    if (vertex.z() > max_z)
                    {
                        max_z = vertex.z();
                    }
                }
            }
        }

        this->size_y_ = std::ceil((max_y - min_y) / sample_delta) + 1;
        this->size_x_ = std::ceil((max_x - min_x) / sample_delta) + 1;
        this->size_z_ = std::ceil((max_z - min_z) / sample_delta) + 1;

        Eigen::Vector3d origin(min_x, min_y, min_z);
        this->origin = origin;
        this->grid_size = sample_delta;

        // Allocate memory on the heap for the file
        this->data_ = reinterpret_cast<bool ***>(std::malloc(this->size_z_ * sizeof(bool *)));
        for (size_t kk = 0; kk < this->size_z_; ++kk)
        {
            this->data_[kk] = reinterpret_cast<bool **>(std::malloc(this->size_y_ * sizeof(bool *)));
            for (size_t jj = 0; jj < this->size_y_; ++jj)
            {
                this->data_[kk][jj] = reinterpret_cast<bool *>(std::malloc(this->size_x_ * sizeof(bool)));
            }
        }
        this->heap_allocated_ = true;

        const Map3D inflated_map = map.Inflate(safety_bound);

        // Read in file
        for (size_t kk = 0; kk < this->size_z_; ++kk)
        {
            for (size_t jj = 0; jj < this->size_y_; ++jj)
            {
                for (size_t ii = 0; ii < this->size_x_; ++ii)
                {
                    const Point3D p(min_x + ii * sample_delta, min_y + jj * sample_delta, min_z + kk * sample_delta);
                    // True indicates occupied, false indicates free
                    this->data_[kk][jj][ii] = !inflated_map.Contains(p) || !inflated_map.IsFreeSpace(p);
                }
            }
        }
        return true;
    }

    Graph3D OccupancyGrid3D::AsGraph() const
    {
        // Build a 3D array of nodes
        std::shared_ptr<Node3D> node_grid[this->size_z_][this->size_y_][this->size_x_];
        for (size_t kk = 0; kk < this->size_z_; ++kk)
        {
            for (size_t jj = 0; jj < this->size_y_; ++jj)
            {
                for (size_t ii = 0; ii < this->size_x_; ++ii)
                {
                    node_grid[kk][jj][ii] = std::make_shared<Node3D>(Eigen::Matrix<double, 3, 1>(ii, jj, kk));
                }
            }
        }

        std::vector<DirectedEdge3D> edges;
        for (int kk = 0; kk < this->size_z_; ++kk)
        {
            for (int jj = 0; jj < this->size_y_; ++jj)
            {
                for (int ii = 0; ii < this->size_x_; ++ii)
                {
                    if (this->IsOccupied(kk, jj, ii) == true)
                    {
                        continue;
                    }

                    constexpr double ADJACENT_COST = 1.0;
                    constexpr double DIAGONAL_COST = std::sqrt(2);
                    constexpr double DIAG_COST_3D = std::sqrt(3);

                    // constexpr double ADJACENT_COST = 1.0;
                    // constexpr double DIAGONAL_COST = 1.0;
                    // constexpr double DIAG_COST_3D = 1.0;
                    // // 26 nodes surrounding the current one
                    // kk - 1 case:
                    if (kk - 1 >= 0 + EDGE_SAFETY)
                    {
                        // Directly below case
                        edges.emplace_back(node_grid[kk - 1][jj][ii], node_grid[kk][jj][ii], ADJACENT_COST);

                        // Adjacent and below cases
                        if (ii - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj][ii - 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj - 1][ii], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (ii + 1 < -EDGE_SAFETY + this->size_x_)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj][ii + 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj + 1][ii], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }

                        // Below and left cases
                        if (ii - 1 >= 0 + EDGE_SAFETY && jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj - 1][ii - 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }
                        if (ii - 1 >= 0 + EDGE_SAFETY && jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj + 1][ii - 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }

                        // Below and right cases
                        if (ii + 1 < -EDGE_SAFETY + this->size_x_ && jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj - 1][ii + 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }
                        if (ii + 1 < -EDGE_SAFETY + this->size_x_ && jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk - 1][jj + 1][ii + 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }
                    }

                    // Above cases
                    if (kk + 1 < -EDGE_SAFETY + this->size_z_)
                    {
                        // Directly above case // TODO: CHECK THIS

                        edges.emplace_back(node_grid[kk + 1][jj][ii], node_grid[kk][jj][ii], ADJACENT_COST);

                        // Adjacent and above cases
                        if (ii - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj][ii - 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj - 1][ii], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (ii + 1 < -EDGE_SAFETY + this->size_x_)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj][ii + 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj + 1][ii], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }

                        // above and corner cases
                        if (ii - 1 >= 0 + EDGE_SAFETY && jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj - 1][ii - 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }
                        if (ii - 1 >= 0 + EDGE_SAFETY && jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj + 1][ii - 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }

                        if (ii + 1 < -EDGE_SAFETY + this->size_x_ && jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj - 1][ii + 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }
                        if (ii + 1 < -EDGE_SAFETY + this->size_x_ && jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk + 1][jj + 1][ii + 1], node_grid[kk][jj][ii], DIAG_COST_3D);
                        }
                    }
                    // Same level cases
                    {
                        // Adjacents
                        if (ii - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk][jj][ii - 1], node_grid[kk][jj][ii], ADJACENT_COST);
                        }
                        if (jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk][jj - 1][ii], node_grid[kk][jj][ii], ADJACENT_COST);
                        }
                        if (ii + 1 < -EDGE_SAFETY + this->size_x_)
                        {
                            edges.emplace_back(node_grid[kk][jj][ii + 1], node_grid[kk][jj][ii], ADJACENT_COST);
                        }
                        if (jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk][jj + 1][ii], node_grid[kk][jj][ii], ADJACENT_COST);
                        }

                        // diagonals
                        if (ii - 1 >= 0 + EDGE_SAFETY && jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk][jj - 1][ii - 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (ii - 1 >= 0 + EDGE_SAFETY && jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk][jj + 1][ii - 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }

                        if (ii + 1 < -EDGE_SAFETY + this->size_x_ && jj - 1 >= 0 + EDGE_SAFETY)
                        {
                            edges.emplace_back(node_grid[kk][jj - 1][ii + 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                        if (ii + 1 < -EDGE_SAFETY + this->size_x_ && jj + 1 < -EDGE_SAFETY + this->size_y_)
                        {
                            edges.emplace_back(node_grid[kk][jj + 1][ii + 1], node_grid[kk][jj][ii], DIAGONAL_COST);
                        }
                    }
                }
            }
        }
        return Graph3D(edges);
    }
} // namespace game_engine
