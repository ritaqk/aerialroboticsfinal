// Author: Tucker Haydon

#include <cstdlib>
#include <vector>
#include <fstream>
#include "student_autonomy_protocol.h"
#include "occupancy_grid3d.h"
#include "path_info3d.h"
#include "a_star3d.h"
#include "polynomial_solver.h"
#include "polynomial_sampler.h"
#include <stack>

// MESS WITH THESE TO OPTIMIZE!
#define TIME_COST 2.8 // Time between waypoints (may increase)
#define MAX_DIST 6.0  // Max dist between downsampled waypoints
#define DERIV_MIN 3   // Which derivative to minimize. Dont know if 3 or 4 is better
// #define RECALC_PERIOD 5.0
#define CHECK_PERIOD 1.0     // How often to check for death. needs to be low for fast detection, but must avoid false pos
#define SAMPLE_SIZE 0.2      // discretization factor - changing is unlikely to help
#define SAMPLER_FREQUENCY 50 // polynomial smoother sampler frequency
#define MINIMUM_VELOCITY 0.04
#define MAX_POLYNOMIAL_ORDER 12
#define RT_SAFETY 0.35     // Ray trace safety factor
#define RT_MAX_DIST 10.0   // Furthest our rays can trace
#define TRAJ_TOLERANCE 0.3 // How far we can be from where we should be before taking action
#define TIME_OFFSET 0
#define TIME_MULTIPLIER 0.9
// For debug
#define MIN_CHECK_TIME 1.0 // Give it this long to get going
#define MSG_PERIOD 30.0    // How often to print stats
#define INFLATION 0.4
#define ACCELERATION_THRESHOLD 0.01 // take action if the acc_err norm is greater than this
#define DT_CHRONO 10
#define DT_CHRONO_SHORT 2
#define SEGMENT_LENGTH 10
#define STRAIGHT_THRESHOLD 0.02
// #define DEBUG 1 // Enable debug messages
// #define ONRAMP_ON 1
#define CONCURRENT_CALCULATION 1
// #define WIND_MODEL_ON 1
// #define DEBUG 1

#define MAX_ACCELERATION_MAG 0.4
#define MAX_VELOCITY_MAG 2.0
namespace game_engine
{
    /*#region DEBUG*/
    void print_vector(std::string text, Eigen::Vector3d vec)
    {
#ifdef DEBUG
        std::cout << text << vec[0] << "," << vec[1] << "," << vec[2] << std::endl;
#endif
    }
    void print_node(std::string text, std::shared_ptr<Node3D> node)
    {
#ifdef DEBUG
        std::cout << text << "[" << node->Data().transpose() << "]" << std::endl;
#endif
    }
    bool print_path(PathInfo path_info)
    {
#ifdef DEBUG
        std::cout << "======== A* Path in occupancy indices ========" << std::endl;
        for (const std::shared_ptr<Node3D> &node : path_info.path)
        {
            print_node("", node);
        }
#endif
        return true;
    }
    /*#endregion*/

    /*#region CONVERSIONS AND CALCULATIONS*/
    double euclidean_dist(Eigen::Vector3d a, Eigen::Vector3d b)
    {
        return sqrt(
            pow(a[0] - b[0], 2) +
            pow(a[1] - b[1], 2) +
            pow(a[2] - b[2], 2));
    }
    double euclidean_dist(std::shared_ptr<Node3D> &a, std::shared_ptr<Node3D> &b)
    {
        return sqrt(
            pow(a->Data().y() - b->Data().y(), 2) +
            pow(a->Data().x() - b->Data().x(), 2) +
            pow(a->Data().z() - b->Data().z(), 2));
    }
    // Needed to convert things from map coords to occ grid indices
    Eigen::Vector3d convert_from_map(const Eigen::Vector3d &v, const double gridsize, const Eigen::Vector3d &origin)
    {
        Eigen::Vector3d u;
        u = v - origin;
        u.x() = (int)((double)u.x() / gridsize);
        u.y() = (int)((double)u.y() / gridsize);
        u.z() = (int)((double)u.z() / gridsize);
        return u;
    }
    // Needed to convert things from occ grid indices to map coords
    Eigen::Vector3d convert_to_map(std::shared_ptr<Node3D> v, const double gridsize, const Eigen::Vector3d &origin)
    {
        double x = ((double)v->Data().x() * SAMPLE_SIZE) + (double)origin.x();
        double y = ((double)v->Data().y() * SAMPLE_SIZE) + (double)origin.y();
        double z = ((double)v->Data().z() * SAMPLE_SIZE) + (double)origin.z();
        return Eigen::Vector3d(x, y, z);
    }
    // Normalize velocity
    Eigen::Vector3d estimate_dir(Eigen::Vector3d current_vel)
    {
        Eigen::Vector3d dir_vec;
        double norm = current_vel.norm();
        if (norm == 0)
        {
            dir_vec[0] = 0;
            dir_vec[1] = 0;
            dir_vec[2] = 0;
        }
        else
        {
            dir_vec[0] = current_vel[0] / norm;
            dir_vec[1] = current_vel[1] / norm;
            dir_vec[2] = current_vel[2] / norm;
        }

        return dir_vec;
    }
    // Given a trajectory, calculate the index of where we want to be right now
    size_t desired_position_idx(Trajectory &trajectory)
    {
        const auto current_time = std::chrono::system_clock::now();
        const double current_time_float = std::chrono::duration_cast<std::chrono::duration<double>>(
                                              current_time.time_since_epoch())
                                              .count();
        const size_t trajectory_size = trajectory.Size();
        if (trajectory_size == 0)
        {
            std::cout << "Error - traj size is 0" << std::endl;
            return 0; // idk what to actually do here...
        }
        size_t trajectory_idx = 0;
        if (trajectory.Time(trajectory_size - 1) < current_time_float)
        {
            trajectory_idx = trajectory.Size() - 1;
        }
        else
        {
            for (size_t idx = 0; idx < trajectory_size; ++idx)
            {
                if (trajectory.Time(idx) > current_time_float)
                {
                    trajectory_idx = ((idx == 0) ? 0 : idx - 1);
                    break;
                }
            }
        }
        return trajectory_idx;
    }
    // Make sure trajectory won't get rejected
    bool is_trajectory_valid(const Trajectory &trajectory,
                             const Map3D &map)
    {
        const size_t trajectory_size = trajectory.Size();
        // Position constraints
        for (size_t idx = 0; idx < trajectory_size; ++idx)
        {
            const Eigen::Vector3d point = trajectory.Position(idx);
            if (false == map.Contains(point))
            {
                std::cout
                    << "Specified trajectory point ["
                    << point.transpose()
                    << "] exceeded map bounds"
                    << std::endl;
                return false;
            }
            if (false == map.IsFreeSpace(point))
            {
                std::cout
                    << "Specified trajectory point ["
                    << point.transpose()
                    << "] is contained within an obstacle"
                    << std::endl;
                return false;
            }
        }
        // Velocity constraints
        for (size_t idx = 0; idx < trajectory_size; ++idx)
        {
            const Eigen::Vector3d vel = trajectory.Velocity(idx);
            if (MAX_VELOCITY_MAG < vel.norm())
            {
#ifdef DEBUG
                // std::cout
                //     << "Specified trajectory velocity"
                //     << " exceeds maximum velocity constraint of "
                //     << MAX_VELOCITY_MAG
                //     << " m/s"
                //     << std::endl;
#endif
                return false;
            }
        }
        // Mean value theorem for velocity constraints
        for (size_t idx = 0; idx < trajectory_size - 1; ++idx)
        {
            const Eigen::Vector3d current_pos = trajectory.Position(idx);
            const Eigen::Vector3d next_pos = trajectory.Position(idx + 1);
            const double current_time = trajectory.Time(idx);
            const double next_time = trajectory.Time(idx + 1);
            const double mean_value_velocity = ((next_pos - current_pos) / (next_time - current_time)).norm();
            if (MAX_VELOCITY_MAG < mean_value_velocity)
            {
#ifdef DEBUG
                // std::cout
                //     << "Specified mean-value trajectory velocity "
                //     << " exceeds maximum velocity constraint of "
                //     << MAX_VELOCITY_MAG
                //     << " m/s"
                //     << std::endl;
#endif
                return false;
            }
        }
        // Acceleration constraints
        for (size_t idx = 0; idx < trajectory_size; ++idx)
        {
            const Eigen::Vector3d acc = trajectory.Acceleration(idx);
            if (MAX_ACCELERATION_MAG < acc.norm())
            {
#ifdef DEBUG
                // std::cout
                //     << "Specified trajectory acceleration "
                //     << " exceeds maximum acceleration constraint of "
                //     << MAX_ACCELERATION_MAG
                //     << " m/s^2"
                //     << std::endl;
                // std::cout << acc.transpose() << std::endl;
                // std::cout << acc.norm() << std::endl;
#endif

                return false;
            }
        }
        // Mean value theorem for acceleration constraints
        for (size_t idx = 0; idx < trajectory_size - 1; ++idx)
        {
            const Eigen::Vector3d current_vel = trajectory.Velocity(idx);
            const Eigen::Vector3d next_vel = trajectory.Velocity(idx + 1);
            const double current_time = trajectory.Time(idx);
            const double next_time = trajectory.Time(idx + 1);
            const double mean_value_acceleration = ((next_vel - current_vel) / (next_time - current_time)).norm();
            if (MAX_ACCELERATION_MAG < mean_value_acceleration)
            {
#ifdef DEBUG
                // std::cout
                //     << "Specified mean-value trajectory acceleration"
                //     << " exceeds maximum acceleration constraint of "
                //     << MAX_ACCELERATION_MAG
                //     << " m/s"
                //     << std::endl;
#endif
                return false;
            }
        }
        return true;
    }

    // given a trajectory and index, determine if that point lies on a straight line.

    bool is_segment_straight(const Trajectory &trajectory, int idx)
    {
        // Make segment length always even
        Eigen::Vector3d sample_dir = estimate_dir(trajectory.Velocity(idx));

        if (idx > SEGMENT_LENGTH / 2 && idx < trajectory.Size() - SEGMENT_LENGTH / 2)
        {
            for (int ii = idx - SEGMENT_LENGTH / 2; ii < (idx + SEGMENT_LENGTH / 2); ii += 1)
            {
                Eigen::Vector3d dir_vec = estimate_dir(trajectory.Velocity(ii));
                if (euclidean_dist(dir_vec, sample_dir) > STRAIGHT_THRESHOLD)
                {
                    return false;
                }
            }
#ifdef DEBUG
// std::cout << "Straight!" << std::endl;
#endif
            return true;
        }
        else
        {
            // Too soon/too late to tell. Don't assume.
            return false;
        }
    }
    /*#endregion*/

    /*#region PATH MODIFIERS*/
    // SMARTER DOWNSAMPLE
    std::vector<std::shared_ptr<Node3D>> smarter_downsample(std::vector<std::shared_ptr<Node3D>> &map_path)
    {
        std::vector<std::shared_ptr<Node3D>> new_map = {}; // make a deep copy

        int ii = 1;
        new_map.push_back(map_path.at(0));
        while (ii < map_path.size() - 1)
        {
            // incoming direction
            double Adx = map_path.at(ii - 1)->Data().x() - map_path.at(ii)->Data().x();
            double Ady = map_path.at(ii - 1)->Data().y() - map_path.at(ii)->Data().y();
            double Adz = map_path.at(ii - 1)->Data().z() - map_path.at(ii)->Data().z();
            // outgoing direction
            double Bdx = map_path.at(ii)->Data().x() - map_path.at(ii + 1)->Data().x();
            double Bdy = map_path.at(ii)->Data().y() - map_path.at(ii + 1)->Data().y();
            double Bdz = map_path.at(ii)->Data().z() - map_path.at(ii + 1)->Data().z();

            int d_total = 0;
            if (std::abs(Adx - Bdx) > 0.01)
            {
                d_total += 1;
            }
            if (std::abs(Ady - Bdy) > 0.01)
            {
                d_total += 1;
            }
            if (std::abs(Adz - Bdz) > 0.01)
            {
                d_total += 1;
            }

            // This is a corner! keep it!
            if (d_total >= 1)
            {
                new_map.push_back(map_path.at(ii));
            }
            ii += 1;
        }
        // Make sure the last element makes it in
        if ((*new_map.at(new_map.size() - 1) == *map_path.at(map_path.size() - 1)) == 0)
        {
            new_map.push_back(map_path.at(map_path.size() - 1));
        }
        // std::cout << "Size after smarter downsample: " << new_map.size() << std::endl;

        return new_map;
    }

    // SMARTER UPSAMPLE
    std::vector<std::shared_ptr<Node3D>> smarter_upsample(std::vector<std::shared_ptr<Node3D>> &map_path, double dist_threshold)
    {
        std::vector<std::shared_ptr<Node3D>> new_map = {}; // make a deep copy

        // double Adx, Ady, Adz;
        // double Bdx, Bdy, Bdz;
        int ii = 1;
        new_map.push_back(map_path.at(0));
        for (int ii = 1; ii < map_path.size(); ii += 1)
        {
            // Check the distance between this and last point
            double diff = euclidean_dist(map_path.at(ii - 1), map_path.at(ii));
            // std::cout << " diff: " << diff << std::endl;
            // if larger than DIST, insert an intermediate
            if (diff > dist_threshold)
            {
                double int_x = map_path.at(ii - 1)->Data().x() + (map_path.at(ii)->Data().x() - map_path.at(ii - 1)->Data().x()) / 2;
                double int_y = map_path.at(ii - 1)->Data().y() + (map_path.at(ii)->Data().y() - map_path.at(ii - 1)->Data().y()) / 2;
                double int_z = map_path.at(ii - 1)->Data().z() + (map_path.at(ii)->Data().z() - map_path.at(ii - 1)->Data().z()) / 2;
                Eigen::Vector3d intermediate(int_x, int_y, int_z);
                new_map.push_back(std::make_shared<Node3D>(intermediate));
            }
            new_map.push_back(map_path.at(ii));
        }
        // Make sure the last element makes it in
        if ((*new_map.at(new_map.size() - 1) == *map_path.at(map_path.size() - 1)) == 0)
        {
            new_map.push_back(map_path.at(map_path.size() - 1));
        }
        // std::cout << "Size after smarter upsample: " << new_map.size() << std::endl;

        return new_map;
    }

    // Check for any obstruction in the straight line path between parent and child
    bool can_interpolate(std::shared_ptr<Node3D> parent, std::shared_ptr<Node3D> child, const Map3D &map)
    {
        Eigen::Vector3d parent_vec(parent->Data().x(), parent->Data().y(), parent->Data().z());
        Eigen::Vector3d child_vec(child->Data().x(), child->Data().y(), child->Data().z());
        // INSERT CODE HERE FOR CALCULATING THE K RESOLUTION!!!
        double diff = euclidean_dist(parent_vec, child_vec);
        if (diff > RT_MAX_DIST)
        {
            return false;
        }
        double k_res = 0.2 / diff;
        for (double k = 0; k < 1; k += k_res)
        {
            double px = child_vec[0] + k * (parent_vec[0] - child_vec[0]);
            double py = child_vec[1] + k * (parent_vec[1] - child_vec[1]);
            double pz = child_vec[2] + k * (parent_vec[2] - child_vec[2]);
            Eigen::Vector3d point(px, py, pz);
            // check every case within the bounding box
            for (int xx = -1; xx < 2; xx += 1)
            {
                for (int yy = -1; yy < 2; yy += 1)
                {
                    for (int zz = -1; zz < 2; zz += 1)
                    {
                        Eigen::Vector3d point(px + (double)xx * RT_SAFETY, py + (double)yy * RT_SAFETY, pz + (double)zz * RT_SAFETY);
                        Eigen::Vector3d point2(px + (double)xx * RT_SAFETY / 2, py + (double)yy * RT_SAFETY / 2, pz + (double)zz * RT_SAFETY / 2);

                        if (map.Contains(point) == false || map.IsFreeSpace(point) == false || map.Contains(point2) == false || map.IsFreeSpace(point2) == false)
                        {
                            // cannot safely make it from point A to point B linearly
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    // RAY TRACE INSPIRED
    std::vector<std::shared_ptr<Node3D>> interpolate_path(std::vector<std::shared_ptr<Node3D>> &map_path, const Map3D &map)
    {
        if (map_path.size() < 5)
        {
            return map_path;
        }
        std::vector<std::shared_ptr<Node3D>> new_path = {};                // make a deep copy
        new_path.push_back(map_path.at(map_path.size() - 1));              // always keep the first
        std::shared_ptr<Node3D> parent = new_path.at(new_path.size() - 1); // Parent will always be the most recent elem in new
        int child_ind = map_path.size() - 2;
        std::shared_ptr<Node3D> grandchild = map_path.at(child_ind - 1);

        while (child_ind > 1)
        {
            if (can_interpolate(parent, grandchild, map) == true)
            {
                // Skip this child. Decrement the counter.
                child_ind -= 1;
                grandchild = map_path.at(child_ind - 1);
            }
            else
            {
                // we can't delete the child. push into onto new path
                // set the new parent to the child
                new_path.push_back(map_path.at(child_ind));
                parent = new_path.at(new_path.size() - 1);
                child_ind -= 1;
                grandchild = map_path.at(child_ind - 1);
            }
        }

        if ((*new_path.at(0) == *map_path.at(0)) == 0)
        {
            new_path.push_back(map_path.at(0));
        }
        // REVERSE PATH
        std::reverse(new_path.begin(), new_path.end());

#ifdef DEBUG
        // std::cout << "Size after raytrace: " << new_path.size() << std::endl;
#endif
        return new_path;
    }

    double euclidean_dist_ray(Eigen::Vector3d parent_vec, Eigen::Vector3d child_vec, const Map3D &map)
    {
        // INSERT CODE HERE FOR CALCULATING THE K RESOLUTION!!!
        double diff = euclidean_dist(parent_vec, child_vec);
        if (diff > RT_MAX_DIST)
        {
            return INFINITY;
        }
        double k_res = 0.2 / diff;
        for (double k = 0; k < 1; k += k_res)
        {
            double px = child_vec[0] + k * (parent_vec[0] - child_vec[0]);
            double py = child_vec[1] + k * (parent_vec[1] - child_vec[1]);
            double pz = child_vec[2] + k * (parent_vec[2] - child_vec[2]);
            Eigen::Vector3d point(px, py, pz);
            // check every case within the bounding box
            for (int xx = -1; xx < 2; xx += 1)
            {
                for (int yy = -1; yy < 2; yy += 1)
                {
                    for (int zz = -1; zz < 2; zz += 1)
                    {
                        Eigen::Vector3d point(px + (double)xx * RT_SAFETY, py + (double)yy * RT_SAFETY, pz + (double)zz * RT_SAFETY);
                        if (map.Contains(point) == false || map.IsFreeSpace(point) == false)
                        {
                            // cannot safely make it from point A to point B linearly
                            return INFINITY;
                        }
                    }
                }
            }
        }
        return diff;
    }
    /*#endregion*/

    /*#region BIG FUNCTIONS*/
    // Run A* NO THREADING
    void run_astar(Eigen::Vector3d current_pos, Eigen::Vector3d current_goal,
                   std::vector<std::shared_ptr<Node3D>> &map_path,
                   const Graph3D &graph3d, const Eigen::Vector3d &occ_origin,
                   Eigen::Vector3d dir_vec)
    {
        PathInfo path_info;
        AStar3D a_star;
        map_path = {};

        // std::cout << "Running A*" << std::endl;
        print_vector("A* current pos:", current_pos);
        print_vector("A* current goal:", current_goal);

        path_info = a_star.Run(graph3d,
                               std::make_shared<Node3D>(convert_from_map(current_pos, SAMPLE_SIZE, occ_origin)),
                               std::make_shared<Node3D>(convert_from_map(current_goal, SAMPLE_SIZE, occ_origin)),
                               dir_vec[0], dir_vec[1], dir_vec[2]);
        // Convert to map coords for trajectory
        // map_path = {std::make_shared<Node3D>(start)}; // overwrite prev with Starting node
        // map_path = {std::make_shared<Node3D>(new_pos)}; // overwrite prev with Starting node
#ifdef DEBUG
        path_info.details.Print();
#endif
        for (const std::shared_ptr<Node3D> &node : path_info.path)
        {
            std::shared_ptr<Node3D> map_node = std::make_shared<Node3D>(convert_to_map(node, SAMPLE_SIZE, occ_origin));
            map_path.push_back(map_node);
        }
    }

    // Smooth path
    void smooth_path(std::vector<std::shared_ptr<Node3D>> &map_path,
                     TrajectoryVector3D &trajectory_vector,
                     const std::chrono::time_point<std::chrono::system_clock> current_chrono_time,
                     double &time_cost, const Map3D &map)
    {
#ifdef DEBUG
        std::ofstream orig_wp_map;
        orig_wp_map.open("orig_wp_map.csv");
        for (int ii = 0; ii < map_path.size(); ii += 1)
        {
            orig_wp_map << map_path.at(ii)->Data().x() << ","
                        << map_path.at(ii)->Data().y() << ","
                        << map_path.at(ii)->Data().z() << "\n";
        }
        orig_wp_map.close();
#endif

        std::vector<double> times = {0};
        std::vector<p4::NodeEqualityBound> node_equality_bounds = {};
        std::vector<p4::NodeInequalityBound> node_inequality_bounds = {};

        std::vector<p4::SegmentInequalityBound> segment_inequality_bounds = {};
        std::vector<std::shared_ptr<Node3D>> new_map;

        if (map_path.size() > 2)
        {
            new_map = smarter_downsample(map_path);
            new_map = smarter_upsample(new_map, 0.5); // TWEAK THESE TO OPTIMIZE
            // new_map = smarter_upsample(new_map, 4);
            new_map = interpolate_path(new_map, map);
            new_map = smarter_upsample(new_map, 1); // TWEAK THESE TO OPTIMIZE
            // new_map = smarter_upsample(new_map, 1); // TWEAK THESE TO OPTIMIZE

            map_path = new_map;
        }

        double highest_dist = 0;
        for (int ii = 1; ii < map_path.size(); ii += 1)
        {
            double diff = euclidean_dist(map_path.at(ii), map_path.at(ii - 1));
            if (diff > highest_dist)
            {
                highest_dist = diff;
            }
        }
        // Go with whichever is higher, for safety
        if (highest_dist / 2 + 1.8 > time_cost)
        {
            time_cost = highest_dist / 2 + 1.8;
        }
        // */

// std::cout << "Using time cost " << time_cost << std::endl;
// std::cout << "Highest diff: " << highest_dist << std::endl;
#ifdef DEBUG
        std::ofstream wps;
        wps.open("wp.csv");
#endif
        for (int ii = 0; ii < map_path.size(); ii += 1)
        {
            float x = map_path[ii]->Data().x();
            float y = map_path[ii]->Data().y();
            float z = map_path[ii]->Data().z();

#ifdef DEBUG
            wps << x << ", " << y << "," << z << "\n";
#endif
            //Position constraint
            node_equality_bounds.push_back(p4::NodeEqualityBound(0, ii, 0, x));
            node_equality_bounds.push_back(p4::NodeEqualityBound(1, ii, 0, y));
            node_equality_bounds.push_back(p4::NodeEqualityBound(2, ii, 0, z));

            // initialize vel/acc
            if (ii == 0)
            {
                node_equality_bounds.push_back(p4::NodeEqualityBound(0, 0, 1, 0));
                node_equality_bounds.push_back(p4::NodeEqualityBound(1, 0, 1, 0));
                node_equality_bounds.push_back(p4::NodeEqualityBound(2, 0, 1, 0));

                node_equality_bounds.push_back(p4::NodeEqualityBound(0, 0, 2, 0));
                node_equality_bounds.push_back(p4::NodeEqualityBound(1, 0, 2, 0));
                node_equality_bounds.push_back(p4::NodeEqualityBound(2, 0, 2, 0));
            }
            else
            {
                times.push_back((double)time_cost * ii);
            }
        }
#ifdef DEBUG
        wps.close();
#endif
        // ready to use the polynomial solver
        // Options to configure the polynomial solver with
        p4::PolynomialSolver::Options solver_options;

        if (map_path.size() < MAX_POLYNOMIAL_ORDER)
        {
            solver_options.polynomial_order = MAX_POLYNOMIAL_ORDER;
        }
        else
        {
            solver_options.polynomial_order = map_path.size(); // Fit an 12th-order polynomial
        }
        solver_options.num_dimensions = 3;           // 3D
        solver_options.continuity_order = 6;         // Require continuity to the 4th order
        solver_options.derivative_order = DERIV_MIN; // minimize snap

        osqp_set_default_settings(&solver_options.osqp_settings);
        solver_options.osqp_settings.polish = true;   // Polish the solution, getting the best answer possible
        solver_options.osqp_settings.verbose = false; // Suppress the printout

        // Use p4::PolynomialSolver object to solve for polynomial trajectories
        p4::PolynomialSolver solver(solver_options);
        const p4::PolynomialSolver::Solution poly_path = solver.Run(
            times,
            node_equality_bounds,
            node_inequality_bounds,
            segment_inequality_bounds);

        // Options to configure the polynomial sampler with
        static p4::PolynomialSampler::Options pos_options;
        static p4::PolynomialSampler::Options vel_options;
        static p4::PolynomialSampler::Options acc_options;

        pos_options.frequency = SAMPLER_FREQUENCY; // Number of samples per second
        pos_options.derivative_order = 0;          // Derivative to sample (0 = pos)

        vel_options.frequency = SAMPLER_FREQUENCY; // Number of samples per second
        vel_options.derivative_order = 1;          // Derivative to sample (0 = pos)

        acc_options.frequency = SAMPLER_FREQUENCY; // Number of samples per second
        acc_options.derivative_order = 2;          // Derivative to sample (0 = pos)

        // Use this object to sample a trajectory
        static p4::PolynomialSampler pos_sampler(pos_options);
        Eigen::MatrixXd pos_samples = pos_sampler.Run(times, poly_path);

        static p4::PolynomialSampler vel_sampler(vel_options);
        Eigen::MatrixXd vel_samples = vel_sampler.Run(times, poly_path);

        static p4::PolynomialSampler acc_sampler(acc_options);
        Eigen::MatrixXd acc_samples = acc_sampler.Run(times, poly_path);

        // TrajectoryVector3D is an std::vector object defined in the trajectory.h
        // file. It's aliased for convenience.

        const std::chrono::milliseconds dt_chrono = std::chrono::milliseconds(DT_CHRONO);
        const double dt = std::chrono::duration_cast<std::chrono::duration<double>>(dt_chrono).count();
        for (size_t idx = 0; idx < pos_samples.cols(); ++idx)
        {
            // chrono::duration<double> maintains high-precision floating point time in seconds
            // use the count function to cast into floating point
            const std::chrono::duration<double> flight_chrono_time =
                current_chrono_time.time_since_epoch() + idx * dt_chrono;
            const double time = flight_chrono_time.count();

            double x = pos_samples(1, idx);
            double y = pos_samples(2, idx);
            double z = pos_samples(3, idx);

            double vx = vel_samples(1, idx);
            double vy = vel_samples(2, idx);
            double vz = vel_samples(3, idx);

            double ax = acc_samples(1, idx);
            double ay = acc_samples(2, idx);
            double az = acc_samples(3, idx);

            // double yaw = 3.1415926 / 4;
            double yaw = 0;

            trajectory_vector.push_back(
                (Eigen::Matrix<double, 11, 1>() << x, y, z,
                 vx, vy, vz,
                 ax, ay, az,
                 yaw,
                 time - TIME_OFFSET)
                    .finished());
        }

    } // namespace game_engine

    // Given an A* path, generate the trajectory
    void generate_trajectory(std::vector<std::shared_ptr<Node3D>> &map_path,
                             Graph3D &graph3d, Eigen::Vector3d &occ_origin,
                             const Map3D &map, std::string &quad_name,
                             std::unordered_map<std::string, Trajectory> &trajectory_map)
    {
        double g_time_cost = TIME_COST; // reset the time cost. TODO: CHECK THIS!!!
        bool is_valid = false;
        TrajectoryVector3D new_traj_vec;
        const std::chrono::time_point<std::chrono::system_clock> current_chrono_time = std::chrono::system_clock::now(); // gonna be updating later anyways...

        do
        {
            TrajectoryVector3D trajectory_vector;
            smooth_path(map_path, trajectory_vector, current_chrono_time, g_time_cost, map);
            Trajectory trajectory_tmp(trajectory_vector);
            is_valid = is_trajectory_valid(trajectory_tmp, map);
            if (is_valid == false)
            {
                g_time_cost += 0.1;
                // std::cout << g_time_cost << std::endl;
            }
            else
            {
                // adjust times
                const std::chrono::time_point<std::chrono::system_clock> new_chrono_time = std::chrono::system_clock::now();
                const std::chrono::milliseconds dt_chrono = std::chrono::milliseconds(DT_CHRONO);
                const double dt = std::chrono::duration_cast<std::chrono::duration<double>>(dt_chrono).count();

#ifdef DEBUG
                std::ofstream traj;
                traj.open("traj.csv");

                std::ofstream vel;
                vel.open("vel.csv");

                std::ofstream acc;
                acc.open("acc.csv");
#endif
                std::chrono::duration<double> last_time = std::chrono::system_clock::now().time_since_epoch();
                for (int ii = 0; ii < trajectory_tmp.Size(); ii += 1)
                {

                    std::chrono::milliseconds dt_chrono = std::chrono::milliseconds(DT_CHRONO);
                    // Classify segment. Is it straight or not?
                    if (is_segment_straight(trajectory_tmp, ii) == true)
                    {
                        std::chrono::milliseconds dt_chrono = std::chrono::milliseconds(DT_CHRONO_SHORT);
                    }
                    std::chrono::duration<double> flight_chrono_time = last_time + dt_chrono;
                    double time = flight_chrono_time.count();
                    last_time = flight_chrono_time;

                    Eigen::Matrix<double, 11, 1> sample = trajectory_tmp.PVAYT(ii);
                    sample[10] = time - TIME_OFFSET;
                    new_traj_vec.push_back(sample);
#ifdef DEBUG
                    traj
                        << sample[0] << "," << sample[1] << "," << sample[2] << "\n";
                    vel << sample[3] << "," << sample[4] << "," << sample[5] << "\n";
                    acc << sample[6] << "," << sample[7] << "," << sample[8] << "\n";
#endif
                }
#ifdef DEBUG
                traj.close();
                vel.close();
                acc.close();
#endif
            }
        } while (is_valid == false);

        Trajectory trajectory(new_traj_vec);
        trajectory_map[quad_name] = trajectory;
    }

    /*#endregion*/

    /*************************************************************/
    /*************************************************************/
    /************     STUDENT AUTONOMY PROTOCOL    ***************/
    /*************************************************************/
    /*************************************************************/
    std::unordered_map<std::string, Trajectory> StudentAutonomyProtocol::UpdateTrajectories()
    {
        /* #region USEFUL INFO ON EVERY ITERATION */
        static double g_time_cost = TIME_COST;
        std::unordered_map<std::string, Trajectory> trajectory_map;
        static const std::string &quad_name = this->friendly_names_[0];
        Eigen::Vector3d current_pos;
        Eigen::Vector3d current_vel;
        static Eigen::Vector3d prev_pos;
        static Eigen::Vector3d prev_vel;
        this->snapshot_->Position(quad_name, current_pos);
        this->snapshot_->Velocity(quad_name, current_vel);
        static std::chrono::time_point<std::chrono::system_clock> last_msg_time = std::chrono::system_clock::now();
        static std::chrono::time_point<std::chrono::system_clock> last_check_time = std::chrono::system_clock::now();
        static std::chrono::time_point<std::chrono::system_clock> last_wind_time = std::chrono::system_clock::now();

        /* #endregion */

        /* #region STATICS */
        static volatile bool a_star_finished = false; // the a_star thread is not finished
        static std::vector<std::shared_ptr<Node3D>> next_map_path = {};
        static std::ofstream actual;
        static std::ofstream wind_est;

        static bool adjusted = false; // flag to say if the time step was wind adjusted
        Eigen::Vector3d dir_vec;
        static Graph3D graph3d = {};
        static bool first_iter = true;
        static bool done_flag = false;
        static bool recalculate = true; // flag to recalculate
        static std::shared_ptr<Node3D> begin_ptr;
        static Eigen::Vector3d current_goal; // red, blue, or begin
        static std::shared_ptr<Node3D> red;
        static std::shared_ptr<Node3D> blue;
        static Eigen::Vector3d occ_origin;
        static std::stack<Eigen::Vector3d> goals;
        static std::vector<std::shared_ptr<Node3D>> map_path;

        static Eigen::Vector3d red_balloon_pos = this->red_balloon_position_;
        static Eigen::Vector3d blue_balloon_pos = this->blue_balloon_position_;
        static Eigen::Vector3d start_pos = current_pos;
        static Eigen::Vector3d zeros(0, 0, 0);

        static std::vector<std::shared_ptr<Node3D>>
            orig_to_red;
        static std::vector<std::shared_ptr<Node3D>> red_to_blue;

        const std::chrono::milliseconds T_chrono = std::chrono::seconds(30);
        const std::chrono::milliseconds dt_chrono = std::chrono::milliseconds(15);
        const double dt = std::chrono::duration_cast<std::chrono::duration<double>>(dt_chrono).count();

        static double red_pop_time;
        static double blue_pop_time;
        static double return_time;

        static const std::chrono::time_point<std::chrono::system_clock> start_chrono_time = std::chrono::system_clock::now();
        static const std::chrono::time_point<std::chrono::system_clock> end_chrono_time = start_chrono_time + T_chrono;
        const std::chrono::time_point<std::chrono::system_clock> current_chrono_time = std::chrono::system_clock::now();
        const std::chrono::duration<double> remaining_chrono_time = end_chrono_time - current_chrono_time;
        const std::chrono::duration<double> flight_chrono_time = current_chrono_time - start_chrono_time;
        const double flight_time = flight_chrono_time.count();
        /* #endregion */

        /*#region FIRST ITERATION*/
        if (first_iter == true)
        {
            actual.open("actual.csv");
            wind_est.open("wind_est.csv");
            // Initialize the map
            std::cout << "First iter" << std::endl;
            OccupancyGrid3D occ_grid;
            occ_grid.LoadFromMap(this->map3d_, SAMPLE_SIZE, INFLATION);
            occ_origin = occ_grid.Origin();
            graph3d = occ_grid.AsGraph();

            begin_ptr = std::make_shared<Node3D>(convert_from_map(current_pos, SAMPLE_SIZE, occ_origin));
            red = std::make_shared<Node3D>(convert_from_map(red_balloon_pos, SAMPLE_SIZE, occ_origin));
            blue = std::make_shared<Node3D>(convert_from_map(blue_balloon_pos, SAMPLE_SIZE, occ_origin));

            // Figure out which is closer
            goals.push(start_pos); // Push the origin first
            double red_cost = (red_balloon_pos - current_pos).norm();
            double blue_cost = (blue_balloon_pos - current_pos).norm();

            if (red_cost <= blue_cost)
            {
                std::cout << "Red first" << std::endl;
                goals.push(blue_balloon_pos);
                goals.push(red_balloon_pos);
            }
            else
            {
                std::cout << "Blue first" << std::endl;
                goals.push(red_balloon_pos);
                goals.push(blue_balloon_pos);
            }

            current_goal = goals.top();
            goals.pop();
            first_iter = false;
        }
        /* #endregion */

        /*#region TELEPORTATION*/
        if (this->red_balloon_status_->position != zeros || this->blue_balloon_status_->position != zeros)
        {
            if (current_goal != start_pos &&
                (this->red_balloon_status_->position != red_balloon_pos || this->blue_balloon_status_->position != blue_balloon_pos))
            {

                // If neither balloon is popped, reevaluate which to go for first.
                if (this->red_balloon_status_->popped == false && this->blue_balloon_status_->popped == false)
                {
                    std::cout << "Teleportation occured. Recalculating for both" << std::endl;
                    if (this->red_balloon_status_->position != zeros)
                    {
                        red_balloon_pos = this->red_balloon_status_->position;
                    }
                    if (this->blue_balloon_status_->position != zeros)
                    {
                        blue_balloon_pos = this->blue_balloon_status_->position;
                    }
                    while (goals.size() > 0)
                    { // clear goals
                        goals.pop();
                    }
                    next_map_path = {};    // clear next path
                    goals.push(start_pos); // Push the origin first
                    double red_cost = (red_balloon_pos - current_pos).norm();
                    double blue_cost = (blue_balloon_pos - current_pos).norm();

                    if (red_cost <= blue_cost)
                    {
                        std::cout << "Red first" << std::endl;
                        goals.push(blue_balloon_pos);
                        goals.push(red_balloon_pos);
                    }
                    else
                    {
                        std::cout << "Blue first" << std::endl;
                        goals.push(red_balloon_pos);
                        goals.push(blue_balloon_pos);
                    }

                    current_goal = goals.top();
                    goals.pop();
                    recalculate = true;
                    this->snapshot_->Position(quad_name, current_pos);

                    std::cout << "temp traj" << std::endl;
                    std::vector<std::shared_ptr<Node3D>> tmp_path;
                    tmp_path.push_back(std::make_shared<Node3D>(current_pos));
                    tmp_path.push_back(std::make_shared<Node3D>(current_pos));
                    generate_trajectory(tmp_path, graph3d, occ_origin, this->map3d_, this->friendly_names_[0], trajectory_map);
                    return trajectory_map;
                }
                else
                {
                    // We were going to red but it teleported
                    if (this->red_balloon_status_->position != red_balloon_pos && this->red_balloon_status_->position != zeros && this->red_balloon_status_->popped == false)
                    {
                        // Update balloon pos and goal
                        red_balloon_pos = this->red_balloon_status_->position;
                        current_goal = red_balloon_pos;
                        // No need to clear goals. Do need to clear next map path
                        next_map_path = {}; // clear next path

                        std::cout << "Red teleported. Rerouting..." << std::endl;
                        recalculate = true;
                        std::cout << "temp traj" << std::endl;
                        this->snapshot_->Position(quad_name, current_pos);

                        std::vector<std::shared_ptr<Node3D>> tmp_path;
                        tmp_path.push_back(std::make_shared<Node3D>(current_pos));
                        tmp_path.push_back(std::make_shared<Node3D>(current_pos));
                        generate_trajectory(tmp_path, graph3d, occ_origin, this->map3d_, this->friendly_names_[0], trajectory_map);
                        return trajectory_map;
                    }
                    // We were going to blue but it teleported
                    if (this->blue_balloon_status_->position != blue_balloon_pos && this->blue_balloon_status_->position != zeros && this->blue_balloon_status_->popped == false)
                    {
                        // Update balloon pos and goal
                        blue_balloon_pos = this->blue_balloon_status_->position;
                        current_goal = blue_balloon_pos;
                        // No need to clear goals. Do need to clear next map path
                        next_map_path = {}; // clear next path
                        std::cout << "Blue teleported. Rerouting..." << std::endl;
                        recalculate = true;
                        this->snapshot_->Position(quad_name, current_pos);

                        std::cout << "temp traj" << std::endl;
                        std::vector<std::shared_ptr<Node3D>> tmp_path;
                        tmp_path.push_back(std::make_shared<Node3D>(current_pos));
                        tmp_path.push_back(std::make_shared<Node3D>(current_pos));
                        generate_trajectory(tmp_path, graph3d, occ_origin, this->map3d_, this->friendly_names_[0], trajectory_map);
                        return trajectory_map;
                    }
                }
            }
        }

        /*#endregion*/

#ifdef CONCURRENT_CALCULATION
        if (recalculate == false && next_map_path.size() == 0 && goals.size() > 0)
        {
            Trajectory current_traj;
            this->trajectory_warden_out_->Read(quad_name, current_traj);
            Eigen::Vector3d last_vel = current_traj.Velocity(current_traj.Size() - 1);
            run_astar(current_goal, goals.top(), next_map_path, graph3d, occ_origin, estimate_dir(last_vel));
        }
#endif

        /* #region GOALS */
        double goal_diff = euclidean_dist(current_pos, current_goal);
        if (goal_diff < 0.2)
        {
            /*#region GOING HOME*/
            if (current_goal != blue_balloon_pos && current_goal != red_balloon_pos)
            {
                if (done_flag == false)
                {
                    std::cout << "*********** SUMMARY ***********" << std::endl;
                    std::cout << "Red time:   " << red_pop_time << std::endl;
                    std::cout << "Blue time:  " << blue_pop_time << std::endl;
                    std::cout << "Round time: " << flight_time << std::endl;
                    std::cout << "*******************************" << std::endl;
                    done_flag = true;
                    actual.close();
                }
                return trajectory_map;
            }
            /*#endregion */
            // TODO: CHANGE THESE BACK ONCE DANIEL FIXES POPPING... ALSO NEED TO ADD IN TELEPORTING
            /*#region GOT RED*/
            if (this->red_balloon_status_->popped == true && current_goal == red_balloon_pos)
            {
                std::cout << "Red Popped! Time: " << flight_time << std::endl;
                red_pop_time = flight_time;
                current_goal = goals.top();
                goals.pop();
                if (next_map_path.size() > 1)
                {
                    // pre calculated the path. Generate trajectory.
                    generate_trajectory(next_map_path, graph3d, occ_origin, this->map3d_, this->friendly_names_[0], trajectory_map);
                    next_map_path = {};
                }
                else
                {
                    //go calculate it now
                    recalculate = true;
                }
            }
            /*#endregion*/

            /*#region GOT BLUE*/
            if (this->blue_balloon_status_->popped == true && current_goal == blue_balloon_pos)
            {
                std::cout << "Blue Popped! Time: " << flight_time << std::endl;
                blue_pop_time = flight_time;
                current_goal = goals.top();
                goals.pop();
                if (next_map_path.size() > 1)
                {
                    // pre calculated the path. Generate trajectory.
                    generate_trajectory(next_map_path, graph3d, occ_origin, this->map3d_, this->friendly_names_[0], trajectory_map);
                    next_map_path = {};
                }
                else
                {
                    //go calculate it now
                    recalculate = true;
                }
            }
            /*#endregion*/
        }
/* #endregion */

/*#region WIND MODEL*/
#ifdef WIND_MODEL_ON
        if (recalculate == false)
        {
            Trajectory current_traj;
            this->trajectory_warden_out_->Read(quad_name, current_traj);
            int cur_idx = desired_position_idx(current_traj);
            Eigen::Vector3d des_pos_cur = current_traj.Position(cur_idx);
            Eigen::Vector3d des_vel_cur = current_traj.Velocity(cur_idx);
            Eigen::Vector3d des_acc_prev = current_traj.Acceleration(cur_idx);
            if (cur_idx > 0)
            {
                des_acc_prev = current_traj.Acceleration(cur_idx - 1);
            }
            Eigen::Vector3d est_acc_prev = ((current_pos - prev_pos) - (prev_vel / (double)SAMPLER_FREQUENCY)) / (1.0 / (double)SAMPLER_FREQUENCY); // dv/dt
            // solve for acceleration estimate using position and velocty

            Eigen::Vector3d acc_err = des_acc_prev - est_acc_prev;

            std::chrono::duration<double> time_since_wind = std::chrono::system_clock::now() - last_wind_time;

            // Estimate the wind every second and record it
            // if (time_since_wind.count() > 1)
            // {
#ifdef DEBUG
            wind_est << acc_err[0] << "," << acc_err[1] << "," << acc_err[2] << "\n";
            last_wind_time = std::chrono::system_clock::now();
#endif
            // }
        }
        /*#endregion*/
#endif
// ONRAMP
#ifdef ONRAMP_ON
        if (recalculate == false)
        {
            // Check our position on the trajectory
            // Read the most current trajectory
            Trajectory current_traj;
            this->trajectory_warden_out_->Read(quad_name, current_traj);
            int cur_idx = desired_position_idx(current_traj);
            Eigen::Vector3d desired_cur_pos = current_traj.Position(cur_idx);
            double error = euclidean_dist(current_pos, desired_cur_pos);
            std::cout << error << std::endl;
            if (error > TRAJ_TOLERANCE)
            {
                std::cout << "Got blown off course!" << std::endl;
                double fwd_error = euclidean_dist(current_pos, current_traj.Position(cur_idx + 1));
                double bwd_error = euclidean_dist(current_pos, current_traj.Position(cur_idx - 1));
                if (fwd_error < bwd_error)
                {
                    // It went forward
                    // ~gradient descent~ ish lol
                    double prev_fwd_err = fwd_error;
                    int ii = 2;
                    while (prev_fwd_err >= fwd_error)
                    {
                        ii += 1;
                        fwd_error = euclidean_dist_ray(current_pos, current_traj.Position(cur_idx + ii), this->map3d_);
                    }
                    // Make a new trajectory to submit without the missing samples
                    TrajectoryVector3D new_traj_vec;

                    for (int idx = ii; idx < current_traj.Size(); idx += 1)
                    {
                        new_traj_vec.push_back(
                            (Eigen::Matrix<double, 11, 1>() << current_traj.PVAYT(idx))
                                .finished());
                    }
                    // closest idx = ii
                    Trajectory trajectory(new_traj_vec);
                    trajectory_map[quad_name] = trajectory;
                    return trajectory_map;
                }
                // Otherwise, it went backward. Just let the PID take the wheel.
            }
        }
#endif
#ifdef DEBUG
        actual << current_pos[0] << "," << current_pos[1] << "," << current_pos[2] << "\n";
#endif

        /* #region DEBUG/RECALC */
        if (recalculate == false)
        {
            // Occasionally sample pos and vel
            std::chrono::duration<double> time_since_check = std::chrono::system_clock::now() - last_check_time;
            std::chrono::duration<double> time_since_msg = std::chrono::system_clock::now() - last_msg_time;

            // Make sure we're still alive!
            if (time_since_check.count() > CHECK_PERIOD && flight_time > MIN_CHECK_TIME)
            {
                double vel_mag = current_vel.norm();
                if (vel_mag < MINIMUM_VELOCITY)
                {
                    // EMERGENCY RECALCULATION!!!
#ifdef DEBUG
                    std::cout << "Looks like I died T__T" << std::endl;
#endif
                    recalculate = true;
                }
                last_check_time = std::chrono::system_clock::now();
            }

            if (time_since_msg.count() > MSG_PERIOD)
            {
                double vel_mag = current_vel.norm();

                std::cout << "Pos :["
                          << current_pos[0] << ", "
                          << current_pos[1] << ", "
                          << current_pos[2] << "]"
                          << " Vel mag: " << vel_mag
                          << ", Time: " << flight_time << std::endl;
                last_msg_time = std::chrono::system_clock::now();
            }
        }
        /* #endregion */
        /*#region RECALCULATE TRAJECTORY*/
        if (recalculate == true)
        {
            std::cout << "recalculating..." << std::endl;
            // calculate the position
            this->snapshot_->Position(quad_name, current_pos); // load in pos again
            this->snapshot_->Velocity(quad_name, current_vel); // load in vel again
            Eigen::Vector3d dir_vec = estimate_dir(current_vel);
            run_astar(current_pos, current_goal, map_path, graph3d, occ_origin, dir_vec);
            generate_trajectory(map_path, graph3d, occ_origin, this->map3d_, this->friendly_names_[0], trajectory_map);
            recalculate = false;
            std::cout << "finished recalculating" << std::endl;
        }
        /*#endregion*/

        prev_pos = current_pos;
        prev_vel = current_vel;
        return trajectory_map;
    }
} // namespace game_engine
