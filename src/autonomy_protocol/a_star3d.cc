// Author: Rita Kambil, Rebecca Wang, Emma Bryant

#include <queue>
#include <iostream>
#include <algorithm>
#include <fstream>
#include "a_star3d.h"
#include <unordered_map>

#define TURN_PENALTY 1.2
// #define DEBUG 1
namespace game_engine
{
    // Anonymous namespace. Put any file-local functions or variables in here
    namespace
    {
        // Helper struct. Functions as a linked list with data. The linked list
        // represents a path. Data contained includes a node and a cost to reach
        // that node.
        struct NodeWrapper
        {
            std::shared_ptr<struct NodeWrapper> parent;
            std::shared_ptr<Node3D> node_ptr;

            // True cost to this node
            double cost;

            // Heuristic to end node
            double heuristic;

            // Equality operator
            bool operator==(const NodeWrapper &other) const
            {
                return *(this->node_ptr) == *(other.node_ptr);
            }

        };

        using NodeWrapperPtr = std::shared_ptr<NodeWrapper>;

        bool already_exists(NodeWrapperPtr node_to_explore, std::vector<NodeWrapperPtr> explored)
        {
            //for(const std::shared_ptr<Node3D> node: explored)
            for (auto node : explored)
            {
                if (*node_to_explore == *node)
                {
                    return true;
                }
            }
            return false;
        }

        // Helper function. Compares the values of two NodeWrapper pointers.
        // Necessary for the priority queue.
        bool NodeWrapperPtrCompare(
            const std::shared_ptr<NodeWrapper> &lhs,
            const std::shared_ptr<NodeWrapper> &rhs)
        {
            return lhs->cost + lhs->heuristic > rhs->cost + rhs->heuristic;
        }

        ///////////////////////////////////////////////////////////////////
        // EXAMPLE HEURISTIC FUNCTION
        // YOU WILL NEED TO MODIFY THIS OR WRITE YOUR OWN FUNCTION
        ///////////////////////////////////////////////////////////////////
        double Heuristic(
            const std::shared_ptr<Node3D> &neighbor,
            const std::shared_ptr<Node3D> &end_ptr)
        {
            int x1 = neighbor->Data().x();
            int y1 = neighbor->Data().y();
            int z1 = neighbor->Data().z();
            int x2 = end_ptr->Data().x();
            int y2 = end_ptr->Data().y();
            int z2 = end_ptr->Data().z();

            return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2) + pow(z2 - z1, 2));
        }

        //Added:  Hash function, copied from nodeeigen.h
        class HashFunction
        {
        public:
            std::size_t operator()(const NodeWrapper &n) const
            {
                const auto RoundDouble = [](const double d) {
                    // Multiply by 10,000 and round to the nearest integer
                    return static_cast<std::size_t>(d * 1e4);
                };

                std::size_t seed = (n.node_ptr)->Data().size() * sizeof(double);
                for (size_t idx = 0; idx < (n.node_ptr)->Data().size(); ++idx)
                {
                    seed ^= RoundDouble((n.node_ptr)->Data().data()[idx]) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
                }
                return seed;
            }
        };
    } // namespace

    PathInfo AStar3D::Run(
        const Graph3D &graph,
        const std::shared_ptr<Node3D> start_ptr,
        const std::shared_ptr<Node3D> end_ptr,
        double Adx, double Ady, double Adz)
    {
        using NodeWrapperPtr = std::shared_ptr<NodeWrapper>;

        ///////////////////////////////////////////////////////////////////
        // SETUP
        // DO NOT MODIFY THIS
        ///////////////////////////////////////////////////////////////////
        Timer timer;
        timer.Start();

        // Use these data structures
        std::priority_queue<
            NodeWrapperPtr,
            std::vector<NodeWrapperPtr>,
            std::function<bool(
                const NodeWrapperPtr &,
                const NodeWrapperPtr &)>>
            to_explore(NodeWrapperPtrCompare);

        std::unordered_map<NodeWrapper, bool, HashFunction> explored;

        ///////////////////////////////////////////////////////////////////
        // YOUR WORK GOES HERE
        // SOME EXAMPLE CODE INCLUDED BELOW
        ///////////////////////////////////////////////////////////////////
        // Create a NodeWrapperPtr
        NodeWrapperPtr start_nw = std::make_shared<NodeWrapper>();

        start_nw->parent = nullptr;
        start_nw->node_ptr = start_ptr;
        start_nw->cost = 0;
        start_nw->heuristic = Heuristic(start_ptr, end_ptr);
        to_explore.push(start_nw);

        int num_explored = 0;
        // Create a PathInfo
        PathInfo path_info;
        path_info.details.num_nodes_explored = 0;
        path_info.details.path_length = 0;
        path_info.details.path_cost = 0;
        path_info.path = {};

        // declare node_to_explore
        using NodeWrapperPtr = std::shared_ptr<NodeWrapper>;

        NodeWrapperPtr node_to_explore;
#ifdef DEBUG
        std::ofstream txt;
        txt.open("astar.txt");
#endif
        while (to_explore.size() != 0)
        {
            node_to_explore = to_explore.top();
            to_explore.pop();

            if ((explored.find(*node_to_explore) != explored.end()) && (explored[*node_to_explore] == 1))
            {
                continue;
            }
            if (*(node_to_explore->node_ptr) == *(end_ptr))
            {
                //Done!
                break;
            }
            else
            {
                // mark explored
                explored[*node_to_explore] = 1;
                num_explored += 1;
#ifdef DEBUG
                txt << "Exploring node ["
                    << node_to_explore->node_ptr->Data().x() << ","
                    << node_to_explore->node_ptr->Data().y() << ","
                    << node_to_explore->node_ptr->Data().z() << "]\n";
#endif
                // Iterate through list of neighbors and push into explored list
                for (auto edge : graph.Edges(node_to_explore->node_ptr))
                {
                    std::shared_ptr<Node3D> neighbor_node = edge.Sink();

                    double cost = edge.Cost();
                    double Bdx;
                    double Bdy;
                    double Bdz;
                    if (node_to_explore->parent != nullptr)
                    {
                        Adx = node_to_explore->parent->node_ptr->Data().x() - node_to_explore->node_ptr->Data().x();
                        Ady = node_to_explore->parent->node_ptr->Data().y() - node_to_explore->node_ptr->Data().y();
                        Adz = node_to_explore->parent->node_ptr->Data().z() - node_to_explore->node_ptr->Data().z();
                    }
                    Bdx = node_to_explore->node_ptr->Data().x() - neighbor_node->Data().x();
                    Bdy = node_to_explore->node_ptr->Data().y() - neighbor_node->Data().y();
                    Bdz = node_to_explore->node_ptr->Data().z() - neighbor_node->Data().z();

                    if (Adx != Bdx)
                    {
                        cost *= (abs(Adx - Bdx) * TURN_PENALTY);
                    }
                    if (Ady != Bdy)
                    {
                        cost *= (abs(Ady - Bdy) * TURN_PENALTY);
                    }
                    if (Adz != Bdz)
                    {
                        cost *= (abs(Adz - Bdz) * TURN_PENALTY);
                    }
#ifdef DEBUG
                    txt << "   Neighbor ["
                        << neighbor_node->Data().x() << ","
                        << neighbor_node->Data().y() << ","
                        << neighbor_node->Data().z() << "], diff is ["
                        << abs(Adx - Bdx) << "," //
                        << abs(Ady - Bdy) << ","
                        << abs(Adz - Bdz) << "] "
                        << "costs " << cost << "\n ";
#endif
                    NodeWrapperPtr neighbor_ptr = std::make_shared<NodeWrapper>();

                    neighbor_ptr->parent = node_to_explore;
                    neighbor_ptr->node_ptr = neighbor_node;
                    neighbor_ptr->cost = cost + node_to_explore->cost;
                    neighbor_ptr->heuristic = Heuristic(neighbor_node, end_ptr);
                    to_explore.push(neighbor_ptr);
                }
            }
        }
#ifdef DEBUG
        txt.close();

#endif
        double total = node_to_explore->cost;

        path_info.path.push_back(node_to_explore->node_ptr);
        do
        {
            node_to_explore = node_to_explore->parent;
            path_info.path.push_back(node_to_explore->node_ptr);
        } while (node_to_explore->node_ptr != start_ptr);

        std::reverse(path_info.path.begin(), path_info.path.end());

        path_info.details.num_nodes_explored = num_explored;
        path_info.details.path_length = path_info.path.size();
        path_info.details.path_cost = total;
        path_info.details.run_time = timer.Stop();
        path_info.path = path_info.path;

        // You must return a PathInfo
        return path_info;
    } // namespace game_engine

} // namespace game_engine
