cmake_minimum_required(VERSION 3.5.0)

set(TARGET lib_autonomy_protocol)

find_package (Eigen3 3.3 NO_MODULE)

set(SOURCE_FILES
  example_autonomy_protocol.cc
  game_snapshot.cc
  student_autonomy_protocol.cc
  a_star3d.cc
  occupancy_grid3d.cc
)

add_library(${TARGET} STATIC ${SOURCE_FILES})

target_include_directories(${TARGET} PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${PROJECT_SOURCE_DIR}/src/P4/src)


target_link_libraries(${TARGET} PUBLIC 
  lib_graph
  lib_mediation_layer
  lib_environment
  lib_util
  lib_p4
  Eigen3::Eigen)

set_target_properties(${TARGET} PROPERTIES
  CXX_STANDARD 14
  CXX_STANDARD_REQUIRED YES
  CXX_EXTENSIONS NO
)

target_compile_options(${TARGET} PRIVATE 
  -Wfatal-errors
)
