// Author: Rita Kambil

#include <stack>

#include "depth_first_search2d.h"

namespace game_engine
{
// Anonymous namespace. Put any file-local functions or variables in here
namespace
{
// Helper struct. Functions as a linked list with data. The linked list
// represents a path. Data contained includes a node and a cost to reach
// that node.
struct NodeWrapper
{
    std::shared_ptr<struct NodeWrapper> parent;
    std::shared_ptr<Node2D> node_ptr;
    double cost;

    // Equality operator
    bool operator==(const NodeWrapper &other) const
    {
        return *(this->node_ptr) == *(other.node_ptr);
    }
};
/***********************************************************/
/***************  My helper functions  *********************/
/***********************************************************/

/*
 * Check if the nodewrapperptr is in a vector of nodewrapperptrs
 *
 * @param nw NodeWrapperPtr
 * @param vec Vector of NodeWrapperPtrs

 * @return returns the index if true, -1 if false
 */
int is_member(std::shared_ptr<NodeWrapper> nw, std::vector<std::shared_ptr<NodeWrapper>> vec)
{
    for (int ii = 0; ii < vec.size(); ii += 1)
    {
        if (*vec[ii] == *nw)
        {
            return ii;
        }
    }
    return -1;
}

/*
 * Wrap an edge in an NodeWrapperPtr    
 *
 * @param parent NodeWrapperPtr
 * @param edge DirectedEdge2D

 * @return NodeWrapper for the wrapped edge
*/
std::shared_ptr<NodeWrapper> wrap_edge(std::shared_ptr<struct NodeWrapper> parent, DirectedEdge2D edge)
{
    std::shared_ptr<NodeWrapper> nw_ptr = std::make_shared<NodeWrapper>();
    nw_ptr->parent = parent;
    nw_ptr->node_ptr = edge.Sink();
    nw_ptr->cost = edge.Cost();
    return nw_ptr;
}
/*
 * Make a NodeWrapperPtr    
 *
 * @param parent NodeWrapperPtr
 * @param node_ptr
 * @param cost

 * @return NodeWrapper for the wrapped edge
*/
std::shared_ptr<NodeWrapper> make_NodeWrapperPtr(std::shared_ptr<struct NodeWrapper> parent,
                                                 std::shared_ptr<Node2D> node_ptr,
                                                 double cost)
{
    std::shared_ptr<NodeWrapper> nw_ptr = std::make_shared<NodeWrapper>();
    nw_ptr->parent = parent;
    nw_ptr->node_ptr = node_ptr;
    nw_ptr->cost = 0;
    return nw_ptr;
}

} // namespace

PathInfo DepthFirstSearch2D::Run(const Graph2D &graph,
                                 const std::shared_ptr<Node2D> start_ptr,
                                 const std::shared_ptr<Node2D> end_ptr)
{
    using NodeWrapperPtr = std::shared_ptr<NodeWrapper>;

    ///////////////////////////////////////////////////////////////////
    // SETUP
    // DO NOT MODIFY THIS
    ///////////////////////////////////////////////////////////////////
    Timer timer;
    timer.Start();

    // Use these data structures
    std::stack<NodeWrapperPtr> to_explore;
    std::vector<NodeWrapperPtr> explored;

    ///////////////////////////////////////////////////////////////////
    // YOUR WORK GOES HERE
    // SOME EXAMPLE CODE INCLUDED BELOW
    ///////////////////////////////////////////////////////////////////
    // Initialize first node
    NodeWrapperPtr p_start = make_NodeWrapperPtr(NULL, start_ptr, 0);
    to_explore.push(p_start);
    explored.push_back(p_start);

    while (!to_explore.empty()) // while to_explore is not empty
    {
        // Pop the top ptr
        NodeWrapperPtr nw_vertex = to_explore.top();
        to_explore.pop();

        // get all the edges eminmating from vertex
        const std::vector<DirectedEdge2D> edge_list = graph.Edges(nw_vertex->node_ptr);

        //iterate over eminating edges and wrap neighbor nodes
        for (const DirectedEdge2D &edge : edge_list)
        {
            NodeWrapperPtr nw_neighbor = wrap_edge(nw_vertex, edge);

            // if neighbor not visited
            if (is_member(nw_neighbor, explored) < 0)
            {
                explored.push_back(nw_neighbor);
                to_explore.push(nw_neighbor);
            }
        }
    }

    // Find the sink node
    NodeWrapperPtr p_end_tmp = make_NodeWrapperPtr(NULL, end_ptr, 0);
    int sink_ind = is_member(p_end_tmp, explored);
    NodeWrapperPtr p_cur = explored[sink_ind];

    // Traverse the tree
    std::vector<std::shared_ptr<Node2D>> path;
    double cost = 0;
    while (!(*p_cur == *p_start))
    {
        path.insert(path.begin(), p_cur->node_ptr);
        p_cur = p_cur->parent;
        cost += p_cur->cost;
    }
    path.insert(path.begin(), p_cur->node_ptr);

    // Create a PathInfo
    PathInfo path_info;
    path_info.details.num_nodes_explored = explored.size();
    path_info.details.path_length = path.size();
    path_info.details.path_cost = cost;
    path_info.details.run_time = timer.Stop();
    path_info.path = path;

    // You must return a PathInfo
    return path_info;
}
} // namespace game_engine
