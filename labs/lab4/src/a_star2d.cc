// Author: Tucker Haydon

#include <queue>

#include "a_star2d.h"
#include "node_wrapper.h"
namespace game_engine
{
// Anonymous namespace. Put any file-local functions or variables in here
namespace
{
// Helper struct. Functions as a linked list with data. The linked list
// represents a path. Data contained includes a node and a cost to reach
// that node.
// struct NodeWrapper
// {
//     std::shared_ptr<struct NodeWrapper> parent;
//     std::shared_ptr<Node2D> node_ptr;

//     // True cost to this node
//     double cost;

//     // Heuristic to end node
//     double heuristic;

//     // Equality operator
//     bool operator==(const NodeWrapper &other) const
//     {
//         return *(this->node_ptr) == *(other.node_ptr);
//     }
// };

// Helper function. Compares thse values of two NodeWrapper pointers.
// Necessary for the priority queue. Compares the FSCORES: cost(G) + heuristic (H)
bool NodeWrapperPtrCompare(
    const std::shared_ptr<NodeWrapper> &lhs,
    const std::shared_ptr<NodeWrapper> &rhs)
{
    return lhs->cost + lhs->heuristic > rhs->cost + rhs->heuristic;
}

///////////////////////////////////////////////////////////////////
// EXAMPLE HEURISTIC FUNCTION
// YOU WILL NEED TO MODIFY THIS OR WRITE YOUR OWN FUNCTION
///////////////////////////////////////////////////////////////////
/*
 * Check if the nodewrapperptr is in a vector of nodewrapperptrs
 *
 * @param nw NodeWrapperPtr
 * @param vec Vector of NodeWrapperPtrs

 * @return returns the index if true, -1 if false
 */
int is_member(std::shared_ptr<NodeWrapper> nw, std::vector<std::shared_ptr<NodeWrapper>> vec)
{
    for (int ii = 0; ii < vec.size(); ii += 1)
    {
        if (*vec[ii] == *nw)
        {
            return ii;
        }
    }
    return -1;
}

// just another way to call is_member, for readability
int get_index(std::shared_ptr<NodeWrapper> nw, std::vector<std::shared_ptr<NodeWrapper>> vec)
{
    return is_member(nw, vec);
}

} // namespace

PathInfo AStar2D::Run(
    const Graph2D &graph,
    const std::shared_ptr<Node2D> start_ptr,
    const std::shared_ptr<Node2D> end_ptr)
{
    using NodeWrapperPtr = std::shared_ptr<NodeWrapper>;

    ///////////////////////////////////////////////////////////////////
    // SETUP
    // DO NOT MODIFY THIS
    ///////////////////////////////////////////////////////////////////
    Timer timer;
    timer.Start();

    // Use these data structures
    std::priority_queue<
        NodeWrapperPtr,
        std::vector<NodeWrapperPtr>,
        std::function<bool(
            const NodeWrapperPtr &,
            const NodeWrapperPtr &)>>
        to_explore(NodeWrapperPtrCompare);

    std::vector<NodeWrapperPtr> explored;

    ///////////////////////////////////////////////////////////////////
    // YOUR WORK GOES HERE
    // SOME EXAMPLE CODE INCLUDED BELOW
    ///////////////////////////////////////////////////////////////////
    /* DFS to initialize astar */
    // std::vector<NodeWrapperPtr> explored;
    std::vector<NodeWrapperPtr> to_explore_vec;

    NodeWrapperPtr p_start(new NodeWrapper(NULL, start_ptr, 0, end_ptr));

    to_explore.push(p_start);
    to_explore_vec.push_back(p_start);
    while (!to_explore.empty())
    {
        // pop the element off the top. Will have minimum cost.
        NodeWrapperPtr nw_vertex = to_explore.top();
        to_explore.pop();
        to_explore_vec.erase(to_explore_vec.begin() + get_index(nw_vertex, to_explore_vec));

        explored.push_back(nw_vertex);

        if (*nw_vertex->node_ptr == *end_ptr)
        {
            break;
        }

        // get edges eminating from node
        const std::vector<DirectedEdge2D> edge_list = graph.Edges(nw_vertex->node_ptr);

        //iterate over eminating edges and wrap neighbor nodes
        for (const DirectedEdge2D &edge : edge_list)
        {
            NodeWrapperPtr alt_neighbor(new NodeWrapper(nw_vertex, edge, end_ptr));
            double cost = alt_neighbor->cost;
            // alt_neighbor->cost = path cost thru current
            if (is_member(alt_neighbor, to_explore_vec) != -1)
            {
                int ind = get_index(alt_neighbor, to_explore_vec);
                NodeWrapperPtr og_neighbor = to_explore_vec[ind];
                // Check the current score for that neighbor
                if (alt_neighbor->cost < og_neighbor->cost)
                {
                    // remove og neighbor from open, new path is better.
                    to_explore_vec.erase(to_explore_vec.begin() + ind);
                    std::priority_queue<
                        NodeWrapperPtr,
                        std::vector<NodeWrapperPtr>,
                        std::function<bool(
                            const NodeWrapperPtr &,
                            const NodeWrapperPtr &)>>
                        tmp(NodeWrapperPtrCompare);

                    for (std::size_t kk = 0; kk < to_explore_vec.size(); ++kk)
                    {
                        tmp.push(to_explore_vec[kk]); // replace the PriQ
                    }
                    to_explore = tmp;
                }
            }
            if (is_member(alt_neighbor, explored) != -1)
            {
                int ind = get_index(alt_neighbor, explored);
                NodeWrapperPtr og_neighbor = explored[ind];
                // Check the current score for that neighbor
                if (alt_neighbor->cost < og_neighbor->cost)
                {
                    // remove og neighbor from open, new path is better.
                    explored.erase(explored.begin() + ind);
                }
            }
            if ((is_member(alt_neighbor, explored) == -1) && (is_member(alt_neighbor, to_explore_vec) == -1))
            {

                to_explore.push(alt_neighbor);
                to_explore_vec.push_back(alt_neighbor);
            }
        }
    }

    // Find the sink node
    NodeWrapperPtr p_end_tmp(new NodeWrapper(NULL, end_ptr, 0, end_ptr));
    int sink_ind = is_member(p_end_tmp, explored);
    NodeWrapperPtr p_cur = explored[sink_ind];
    double cost = p_cur->cost;

    // Traverse the tree
    std::vector<std::shared_ptr<Node2D>> path;
    while (p_cur->parent != NULL)
    {
        path.insert(path.begin(), p_cur->node_ptr);
        p_cur = p_cur->parent;
    }
    path.insert(path.begin(), p_cur->node_ptr);

    // Create a PathInfo
    PathInfo path_info;
    path_info.details.num_nodes_explored = explored.size();
    path_info.details.path_length = path.size();
    path_info.details.path_cost = cost;
    path_info.details.run_time = timer.Stop();
    path_info.path = path;

    // You must return a PathInfo
    return path_info;
}

} // namespace game_engine
