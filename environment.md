# Rita's guide to kickass coding

The rules might seem excessive, but trust me it will make your lives a lot easier. I don't mean to be a hard ass, but establishing best practices before starting a group coding project has literally saved me hundreds of hours over the past few semesters.

## VSCode

1. Install my VM from the provided link. This will come with VSCode installed and configured.
2. Use ctrl+` to open the integrated terminal. You can use this exactly the same as you would any other terminal but no gnarly window switching, and a pretty color scheme!
3. When reading .md files, use ctrl+shift+v to read in preview mode for nice formatting.
4. alt+left click to open a definition for a function. This can be kinda dumb sometimes if functions have the same names. Make sure you're looking at the right one if this happens.
5. ctrl+shift+P opens the command palette. nearly everything you could ever want is in here. you start typing, vscode finds it for you. its really great.
6. ctrl-F is find, ctrl-H is find and replace, ctrl+K, B minimizes sidebar. I actually have the keyboard shortcuts set up to mimic sublime.

## Using Git

1. NEVER PUSH TO MASTER UNLESS I CONFIRM IT'S OKAY
2. If you encounter merge conflicts (which shouldn't happen if you follow rule 1) contact me and I will fix it.
3. _Always_ do your work on a branch. Naming convention is "your name"/"feature" Example:

```
    # create a branch
    git checkout -b rita/dijkstra
```

5. When you're done with a feature, or you want someone else to look at it, push your branch:

```
    git push --set-upstream origin/branchname
```

You can then go to the repo on Github. Submit a pull request and I will review your code.

4. All code must be formatted before committing. I don't like whitespace clogging up my diffs. With my extension setup, this should happen automatically on save. If not, use ctrl+shift+i. This will autoindent and prettify your code.

5. Set up your username/SSH keys. Right now it's set up as me. What to do:

```
git config --global user.name "Your name"
git config --global user.email "Your email"

cd ~
rm .ssh*
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
# Save it in default
cat ~/.ssh/id_rsa.pub
```

Copy the output to your clipboard and follow the instructions to add it to Github here: https://help.github.com/en/enterprise/2.15/user/articles/adding-a-new-ssh-key-to-your-github-account

Now you should be okay to push and pull easypeasy.

6. How to pull assignment updates when the TAs announce them:

```
git checkout master
git pull assignment master
git checkout name/branchname
git rebase master
```

7. How to pull updated code from teammates:

```
git checkout master
git pull origin master
git checkout name/branchname
git rebase master

```

# Coding conventions

1. All code must be documented. Follow this template for function documentation:

```C++
/*
 * Function Name
 * Function Description
 *
 * @param name and type, any relevant details
 * @param name and type, any relevant details

 * @return description and type
*/
```

Example:

```C++
/* is_member_coords
 * Check if the specific coords is in a vector of nodewrapperptrs
 *
 * @param x
 * @param y
 * @param vec Vector of NodeWrapperPtrs

 * @return returns the index if true, -1 if false
 */
```

2. I prefer lowercase and underscores when naming functions. This is because it is the most readable format. Please name your functions descriptively and make them readable.

# Running the simulator
I wrote a quick and dirty tmux script to run all the stuff they have in the readme. 
1. Open a new terminal, type roscore, enter, minimize the window. It'll run in the background.
2. Open a new terminal. do:
```
cd Workspace/game-engine
./scripts/run_tmux.sh
```
3. Wait a bit for the whole thing to load. visualizer sometimes takes upwards of 10 seconds to load.
4. You can run the executable of your choice from the first pane of the tmux window!
5. When you want to change something, kill the tmux using tmux kill-session in one of the panes. 

